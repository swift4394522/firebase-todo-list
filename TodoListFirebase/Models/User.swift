//
//  User.swift
//  TodoListFirebase
//
//  Created by Aidan Walker on 20/05/2023.
//

import Foundation

// Codable: gives ability to convert the obj into a dictionary for firebase db
struct User: Codable {
    let id: String
    let name: String
    let email: String
    let joined: TimeInterval
}
