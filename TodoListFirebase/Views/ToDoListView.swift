//
//  ToDoListView.swift
//  TodoListFirebase
//
//  Created by Aidan Walker on 20/05/2023.
//

import FirebaseFirestoreSwift
import SwiftUI

struct ToDoListView: View {
    @StateObject var viewModel: ToDoListViewViewModel
    // @FirestoreQuery - firestore has a handy property wrapper/functionality for observing/listening for entries at a particular path. Query which continously listens for items
    @FirestoreQuery var items: [ToDoListItem]

    init(userId: String) {
        // firebase db - users/<id>/todos/<entries>
        self._items = FirestoreQuery(
            collectionPath: "users/\(userId)/todos"
        )
            // pass in user id so we can listen for todo list items for a particular user
        self._viewModel = StateObject(
            wrappedValue: ToDoListViewViewModel(userId: userId)
        )
    }

    var body: some View {
        NavigationView {
            VStack {
                List(items) { item in
                    ToDoListItemView(item: item)
                        // Swipeable delete button
                        .swipeActions {
                            Button("Delete") {
                                viewModel.delete(id: item.id)
                            }
                            .tint(.red)
                        }
                }
                .listStyle(PlainListStyle())
            }
            .navigationTitle("To Do List")
            .toolbar {
                Button {
                    // Action
                    viewModel.showingNewItemView = true
                } label: {
                    Image(systemName: "plus")
                }

            }
            // displays new todo list item modal
            .sheet(isPresented: $viewModel.showingNewItemView) {
                NewItemView(newItemPresented: $viewModel.showingNewItemView)
            }
        }
    }
}

struct ToDoListItemsView_Previews: PreviewProvider {
    static var previews: some View {
        ToDoListView(userId: "RcNUw88NDCOuzwgBeispLnTTLuv2")
    }
}
