//
//  LoginViewViewModel.swift
//  TodoListFirebase
//
//  Created by Aidan Walker on 20/05/2023.
//
import FirebaseAuth
import Foundation

class LoginViewViewModel: ObservableObject {
    @Published var email = ""
    @Published var password = ""
    @Published var errorMessage = ""

    init() {}

    func login() {
//        guard !email.trimmingCharacters(in: .whitespaces).isEmpty,
//               !password.trimmingCharacters(in: .whitespaces).isEmpty else {
//                   errorMessage = "Please fill in all fields"
//             return
//         }
        
        // if not able to validate stop
        guard validate() else {
            return
        }

        // Attempt firebase auth log in
       Auth.auth().signIn(withEmail: email, password: password)
    }
    
    // validate user
    private func validate() -> Bool {
        errorMessage = ""
        guard !email.trimmingCharacters(in: .whitespaces).isEmpty,
              !password.trimmingCharacters(in: .whitespaces).isEmpty else {
            errorMessage = "Please fill in all fields."
            return false
        }

        guard email.contains("@") && email.contains(".") else {
            errorMessage = "Please enter valid email."
            return false
        }

        return true
    }
}
