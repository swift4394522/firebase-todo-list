//
//  NewItemViewViewModel.swift
//  TodoListFirebase
//
//  Created by Aidan Walker on 20/05/2023.
//

import FirebaseAuth
import FirebaseFirestore
import Foundation

class NewItemViewViewModel: ObservableObject {
    @Published var title = ""
    @Published var dueDate = Date()
    @Published var showAlert = false

    init() {}

    func save() {
        guard canSave else {
            return
        }

        // Get current user id
        guard let uId = Auth.auth().currentUser?.uid else {
            return
        }

        // Create model/instance of todo item
        let newId = UUID().uuidString
        let newItem = ToDoListItem(
            id: newId,
            title: title,
            dueDate: dueDate.timeIntervalSince1970,
            createdDate: Date().timeIntervalSince1970,
            isDone: false
        )

        // Save model
        let db = Firestore.firestore()
        db.collection("users")
            .document(uId)
            .collection("todos")
            .document(newId)
            .setData(newItem.asDictionary())
    }

    // create computed property for validation
    var canSave: Bool {
        guard !title.trimmingCharacters(in: .whitespaces).isEmpty else {
            return false
        }
// 86400(seconds in a day) - subtracts 24 hours from current date(Makes sure the due date is greater than yesterday)
// prevents time zone complexity edge cases
        guard dueDate >= Date().addingTimeInterval(-86400) else {
            return false
        }

        return true
    }
}
