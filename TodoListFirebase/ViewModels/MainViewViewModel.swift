//
//  MainViewViewModel.swift
//  TodoListFirebase
//
//  Created by Aidan Walker on 20/05/2023.
//

import FirebaseAuth
import Foundation

class MainViewViewModel: ObservableObject {
    // Observe from firebase when the current user changes so the View can be updated
    @Published var currentUserId: String = ""
    
    // var listener - nil by default so its made an optional
    private var handler: AuthStateDidChangeListenerHandle?

    init() {
        // observe from firebase if the user changes
        self.handler = Auth.auth().addStateDidChangeListener { [weak self] _, user in
            DispatchQueue.main.async {
                // whenever the user signs in/out, @Published currentUserId will be triggered
                self?.currentUserId = user?.uid ?? ""
            }
        }
    }

    // determines if a user is signed in
    public var isSignedIn: Bool {
        return Auth.auth().currentUser != nil
    }
}
