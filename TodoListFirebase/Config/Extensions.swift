//
//  Extensions.swift
//  TodoListFirebase
//
//  Created by Aidan Walker on 02/06/2023.
//

import Foundation

// Part of the Codable protocol thats responsible for encoding to data
extension Encodable {
    func asDictionary() -> [String: Any] {
        // give back data from the current thing thats codable else return empty dictionary
        guard let data = try? JSONEncoder().encode(self) else {
            return [:]
        }

        // convert data to json
        do {
            let json = try JSONSerialization.jsonObject(with: data) as? [String: Any]
            return json ?? [:]
        } catch {
            return [:]
        }
    }
}
