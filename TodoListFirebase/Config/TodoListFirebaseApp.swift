//
//  TodoListFirebaseApp.swift
//  TodoListFirebase
//
//  Created by Aidan Walker on 20/05/2023.
//

import SwiftUI
import FirebaseCore

// class AppDelegate: NSObject, UIApplicationDelegate {
//  func application(_ application: UIApplication,
//                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
//    FirebaseApp.configure()
//
//    return true
//  }
// }

@main
struct TodoListFirebaseApp: App {
    init() {
        // GoogleService Info contains API KEY and all the configuration from firebase
        FirebaseApp.configure()
    }
    // register app delegate for Firebase setup
    // @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    var body: some Scene {
        WindowGroup {
          //  NavigationView {
            MainView()
        }
    }
}
