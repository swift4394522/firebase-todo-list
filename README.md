<h1 align="center">
<br>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/swift/swift-original.svg" width="80px" />       
<br>
<br>
Todo List - SwiftUI
</h1>

<h4 align="center">MVVM, Firebase with Login Auth</h4>
<p align="center">Firebase Realtime DB, Email/Password Auth, Corresponding Views/View Models, MainView as the entry point</p>

<div align="center">
   <img align="center" src="./TodoListFirebase/Config/todolist.png" width="230px">
   <img align="center" src="./TodoListFirebase/Config/todolist.gif" width="230px">
</div>
